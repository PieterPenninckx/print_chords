# Print Chords

A stand-alone jack application that listens to a midi port and prints the chord names.

## Known limitations

Currently, only the following chord types are supported:
* major (with its inversions)
* minor (with its inversions)
* dominant seventh (with its inversions)
* major seventh (with its inversions)
* minor seventh (with its inversions)
* augmented
* diminished (with its inversions)
* power (with its inversion)
* suspended (suspended fourth and suspended second)

## Installing
`print_chords` has only been tested to work under Linux.
The only installation method currently provided is to compile from source code,
either from `crates.io` or after downloading the source code.

* Install dependencies: install `libjack` and `libjack-dev` or similar from your distro's package manager
* Install rust: install cargo and rustc 1.41.0 or higher from your distro's package manager (not tested) or [install rust using rustup](https://www.rust-lang.org/tools/install)
* Compile and install: either
  * run `cargo install print_chords` OR
  * download the source code of `print_chords`, go to the folder containing the `Cargo.toml` file and run `cargo install`

## Usage
See the [USAGE.txt](USAGE.txt) file.

## Contributing
Contributions are welcome!
See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Re-use of source code
`print_chords` is currently only an application and not a library.
If you want to re-use part of the source code, I would appreciate it if you get in touch,
so that we can discuss how to manage the source code efficiently.

## License

`print_chords` is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version that is approved by Pieter Penninckx.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program, in the file [LICENSE-AGPL-3.0.txt](LICENSE-AGPL-3.0.txt). 
If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
