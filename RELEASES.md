Version 0.1.4
=============
Bug fixes
* fix crash with suspended chords
* fix crash with midi events shorter than 3 bytes

Version 0.1.3
=============
* no longer use the `rsynth` library
* fix crash with power chords
* fix notation of (major/minor/dominant) seventh chords

Version 0.1.2
=============
* upgrade dependencies
* make sure it compiles with rustc 1.41.0 (the version that ships with Debian 10)  
* add support for the following chord types:
  * augmented
  * diminished (with its inversions)
  * power (with its inversion)
  * suspended (suspended fourth and suspended second)
  
Version 0.1.1
=============
Bug fixes:
* Ensure that sharps and flats are consistent, 
  e.g. "E♭m/G♭" is represented as such and not as "E♭m/F♯".

Version 0.1.0
=============
Initial release
