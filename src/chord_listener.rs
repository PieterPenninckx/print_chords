//     A stand-alone jack application that listens to a midi channel and prints the chord names.
//
//     Copyright (C) 2021  Pieter Penninckx
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as
//     published by the Free Software Foundation, either version 3 of the
//     License, or (at your option) any later version that is approved by Pieter Penninckx.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::FIRST_BIT;

use midi_consts::channel_event::control_change::{ALL_NOTES_OFF, ALL_SOUND_OFF};
use midi_consts::channel_event::{
    CONTROL_CHANGE, EVENT_TYPE_MASK, MIDI_CHANNEL_MASK, NOTE_OFF, NOTE_ON,
};

#[derive(Clone, Copy)]
pub struct ChordListener {
    chord: u128,
}

impl ChordListener {
    pub fn new() -> Self {
        Self { chord: 0 }
    }

    pub fn handle_event(&mut self, raw_midi_event: &[u8]) {
        if raw_midi_event.len() != 3 {
            return;
        }
        let note_number = raw_midi_event[1] & 0b01111111;
        match (
            raw_midi_event[0] & EVENT_TYPE_MASK,
            raw_midi_event[1],
            raw_midi_event[2],
        ) {
            (NOTE_OFF, _, _) | (NOTE_ON, _, 0) => {
                self.chord = self.chord & (!(FIRST_BIT >> note_number));
            }
            (NOTE_ON, _, _) => {
                self.chord = self.chord | (FIRST_BIT >> note_number);
            }
            (CONTROL_CHANGE, ALL_NOTES_OFF, 0) | (CONTROL_CHANGE, ALL_SOUND_OFF, 0) => {
                self.chord = 0;
            }
            (_, _, _) => {}
        }
    }
}

pub struct MultiChannelChordListener {
    listeners: Vec<Option<ChordListener>>,
}

impl MultiChannelChordListener {
    pub fn new_with_all_channels() -> Self {
        Self {
            listeners: vec![Some(ChordListener::new()); 16],
        }
    }
    #[allow(unused)]
    pub fn new_with_channel_indices(channel_indices: Vec<u8>) -> Self {
        let mut listeners = Vec::new();
        for index in channel_indices {
            if index > 16 {
                continue;
            }
            while listeners.len() <= index as usize {
                listeners.push(None)
            }
            listeners[index as usize] = Some(ChordListener::new());
        }
        Self { listeners }
    }

    pub fn handle_event(&mut self, raw_midi_event: &[u8]) {
        if raw_midi_event.len() != 3 {
            return;
        }
        let channel = raw_midi_event[0] & MIDI_CHANNEL_MASK;
        if let Some(Some(ref mut listener)) = self.listeners.get_mut(channel as usize) {
            listener.handle_event(raw_midi_event);
        }
    }

    pub fn get_chord(&self) -> u128 {
        let mut result = 0;
        for listener in self.listeners.iter() {
            if let Some(listener) = listener {
                result = result | listener.chord;
            }
        }
        result
    }
}
