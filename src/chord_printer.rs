//     A stand-alone jack application that listens to a midi channel and prints the chord names.
//
//     Copyright (C) 2021  Pieter Penninckx
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as
//     published by the Free Software Foundation, either version 3 of the
//     License, or (at your option) any later version that is approved by Pieter Penninckx.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#[cfg(test)]
use crate::FIRST_BIT;
const FIRST_12_BITS: u128 = (0b1111111111110000 as u128) << (128 - 16);
const FIRST_12_BITS_U32: u32 = (0b1111111111110000 as u32) << (32 - 16);
use enum_primitive::FromPrimitive;

#[cfg(test)]
fn parse<'a>(name: &'a str) -> Option<u128> {
    let mut result = 0;
    for note in name.split(" ") {
        if let (Some(n), remainder) = note_from_name(note) {
            if let Ok(oct) = remainder.parse::<u32>() {
                result |= FIRST_BIT >> (n + oct * 12);
            } else {
                return None;
            }
        } else {
            return None;
        }
    }
    Some(result)
}

fn wrap_chord(chord: u128) -> u16 {
    let mut chord = chord;
    let mut result = ((chord & FIRST_12_BITS) >> 128 - 16) as u16;
    while chord != 0 {
        chord <<= 12;
        result |= ((chord & FIRST_12_BITS) >> 128 - 16) as u16;
    }
    result & 0b1111111111110000
}

#[test]
fn wrap_chord_works() {
    let first_note = (1 as u128).rotate_right(1);
    assert_eq!(wrap_chord(first_note), 0b1000000000000000);
    assert_eq!(
        wrap_chord(first_note | (first_note >> 13)),
        0b1100000000000000
    );
}

fn lowest(chord: u128) -> Option<u32> {
    let zeroes = chord.leading_zeros();
    if zeroes == 128 {
        None
    } else {
        Some(zeroes)
    }
}

const fn repeat(chord: u16) -> u32 {
    let chord = (chord as u32) << 16;
    chord | (chord >> 12)
}

const fn invert(chord: u16) -> u16 {
    let mut chord = repeat(chord) & ((0b0111111111111111 as u32) << 16);
    chord = chord << chord.leading_zeros();
    ((chord & FIRST_12_BITS_U32) >> 16) as u16
}

#[test]
fn invert_works() {
    let observed = invert(MAJOR);
    assert_eq!(observed, 0b1001000010000000);
}

#[rustfmt::skip]
const fn uninvert(chord: u16) -> u16 {
    let chord = repeat(chord);
    //    12 bits     12 bits     8 bits
    //    chord       repeated    padding
    //    11111111111122222222222200000000 Pattern
    //    1001000100001001000*000000000000 Example of repeated chord (highest 1 replaced by *):
    (((
        chord >> (chord.trailing_zeros() - 7)
//        11111111111122222222222200000000 // Pattern
//        000001001000100001001000*0000000 // Example of repeated and shifted chord
    ) & 0b00000000000011111111111100000000) >> 4) as u16
    //    00000000000000001111111111111111 // Bitmask of the last 16 bits
}

#[test]
fn uninvert_works() {
    for chord in vec![MAJOR, MAJOR] {
        assert_eq!(
            chord,
            uninvert(invert(chord)),
            "{:016b}, {:016b}",
            chord,
            uninvert(invert(chord))
        );
        assert_eq!(chord, uninvert(uninvert(invert(invert(chord)))));
        assert_eq!(invert(chord), uninvert(invert(invert(chord))));
    }
}

const fn remove_highest(chord: u16) -> u16 {
    chord & (0b1111111111111110 << chord.trailing_zeros())
}

#[test]
fn remove_highest_works() {
    let a = 1 << 4;
    let b = 1 << 8;
    assert_eq!(remove_highest(a | b), b);
}

#[test]
fn remove_highest_still_works() {
    let c = 0b1000000000000000;
    let a = c >> 9;
    assert_eq!(remove_highest(a | c), c);
}

const fn relative_position_of_highest(chord: u16) -> u32 {
    chord.trailing_zeros() - 3
}

const fn relative_position_of_second_highest(chord: u16) -> u32 {
    relative_position_of_highest(remove_highest(chord))
}

#[test]
fn relative_position_of_second_highest_works() {
    let c = 0b1000000000000000;
    let a = c >> 9;
    assert_eq!(relative_position_of_second_highest(a | c), 12);
}

const fn relative_position_of_third_highest(chord: u16) -> u32 {
    relative_position_of_second_highest(remove_highest(chord))
}

// Adding a new chord type:
// Step 1: Add a bit representation here (do not add the inversions).
// This is a bit-wise representation of the chord,
// from left to right, a bit is 1 if it correspond to a note in the chord and 0 otherwise.
// Example, the C Major chord has the following notes: C, E and G.
// So if we write a 1 under the notes that are part of the chord and a 0 under the notes that are not,
// Then we get the following:
//     C  C♯ D  E♭ E  F  F♯ G  A♭ A  B♭ B (1  2  3  4)
//     1  0  0  0  1  0  0  1  0  0  0  0  0  0  0  0
// Add the end, we add four zeros.
// If the chord exceeds one octave, map the higher notes to the range of the first twelve notes.
#[rustfmt::skip]
mod consts {
    //                          0bC D EF G A B0000
    pub const MAJOR:      u16 = 0b1000100100000000;
    pub const MINOR:      u16 = 0b1001000100000000;
    pub const DOMINANT7:  u16 = 0b1000100100100000;
    pub const MAJOR7:     u16 = 0b1000100100010000;
    pub const MINOR7:     u16 = 0b1001000100100000;
    pub const AUGMENTED:  u16 = 0b1000100010000000;
    pub const DIMINISHED: u16 = 0b1001001000000000;
    pub const POWER:      u16 = 0b1000000100000000;
    pub const SUSPENDED4: u16 = 0b1000010100000000;
    pub const SUSPENDED2: u16 = 0b1010000100000000;
}

use consts::*;

// Adding a new chord type:
// Step 2: Add it to the list below, also add the inversions (if any).
enum_from_primitive! {
    #[derive(Debug, PartialEq, Clone, Copy)]
    #[repr(u16)]
    enum ChordType {
        Major          = MAJOR,
        MajorInv1      = invert(MAJOR),
        MajorInv2      = invert(invert(MAJOR)),
        Minor          = MINOR,
        MinorInv1      = invert(MINOR),
        MinorInv2      = invert(invert(MINOR)),
        Dominant7      = DOMINANT7,
        Dominant7Inv1  = invert(DOMINANT7),
        Dominant7Inv2  = invert(invert(DOMINANT7)),
        Dominant7Inv3  = invert(invert(invert(DOMINANT7))),
        Major7         = MAJOR7,
        Major7Inv1     = invert(MAJOR7),
        Major7Inv2     = invert(invert(MAJOR7)),
        Major7Inv3     = invert(invert(invert(MAJOR7))),
        Minor7         = MINOR7,
        Minor7Inv1     = invert(MINOR7),
        Minor7Inv2     = invert(invert(MINOR7)),
        Minor7Inv3     = invert(invert(invert(MINOR7))),
        Augmented      = AUGMENTED,
        Diminished     = DIMINISHED,
        DiminishedInv1 = invert(DIMINISHED),
        DiminishedInv2 = invert(invert(DIMINISHED)),
        Power          = POWER,
        PowerInv1      = invert(POWER),
        Suspended4     = SUSPENDED4,
        Suspended2     = SUSPENDED2,
        Other          = 0b1111111111111111,
        Silence        = 0,
    }
}

impl ChordType {
    // Adding a new chord type:
    // Step 3: Add the suffix to the list below
    fn suffix(&self) -> &'static str {
        use ChordType::*;
        match self {
            Major | MajorInv1 | MajorInv2 => "",
            Minor | MinorInv1 | MinorInv2 => "m",
            Dominant7 | Dominant7Inv1 | Dominant7Inv2 | Dominant7Inv3 => "7",
            Major7 | Major7Inv1 | Major7Inv2 | Major7Inv3 => "M7",
            Minor7 | Minor7Inv1 | Minor7Inv2 | Minor7Inv3 => "m7",
            Augmented => "+",
            Diminished | DiminishedInv1 | DiminishedInv2 => "dim",
            Power | PowerInv1 => "5",
            Suspended4 => "sus4",
            Suspended2 => "sus2",
            Silence => "NC",
            Other => "???",
        }
    }

    // Adding a new chord type:
    // Step 4: Add the inversions to the list below.
    fn inversion(&self) -> u8 {
        use ChordType::*;
        match self {
            Major | Minor | Dominant7 | Major7 | Minor7 | Augmented | Diminished | Power
            | Suspended4 | Suspended2 | Other | Silence => 0,
            MajorInv1 | MinorInv1 | Dominant7Inv1 | Major7Inv1 | Minor7Inv1 | DiminishedInv1
            | PowerInv1 => 1,
            MajorInv2 | MinorInv2 | Dominant7Inv2 | Major7Inv2 | Minor7Inv2 | DiminishedInv2 => 2,
            Dominant7Inv3 | Major7Inv3 | Minor7Inv3 => 3,
        }
    }

    fn root_difference(&self) -> u32 {
        match self.inversion() {
            0 => 0,
            1 => relative_position_of_highest(*self as u16),
            2 => relative_position_of_second_highest(*self as u16),
            3 => relative_position_of_third_highest(*self as u16),
            _ => unreachable!(),
        }
    }

    fn uninvert(&self) -> Self {
        match self {
            ChordType::Silence => ChordType::Silence,
            ChordType::Other => ChordType::Other,
            c => match c.inversion() {
                0 => *self,
                1 => ChordType::from_u16(uninvert(*self as u16))
                    .expect("First inversion of chord type doesn't appear to be correct."),
                2 => ChordType::from_u16(uninvert(uninvert(*self as u16)))
                    .expect("Second inversion of chord type doesn't appear to be correct."),
                3 => ChordType::from_u16(uninvert(uninvert(uninvert(*self as u16))))
                    .expect("Third inversion of chord type doesn't appear to be correct."),
                _ => unreachable!(),
            },
        }
    }

    // Delta to apply in order to get the relative major chord.
    fn relative_chord(&self) -> u32 {
        match self {
            ChordType::Silence => 0,
            ChordType::Other => 0,
            ChordType::Power | ChordType::PowerInv1 => 0, // This is an indetermined chord, we treat it as if it were a major chord.
            c => match (c.uninvert() as u16) & 0b0001100000000000 {
                0b0000100000000000 => 0, // major chord -> this is already the major chord.
                0b0001000000000000 => 3, // minor chord -> this is a minor chord
                0b0000000000000000 => 0, // This is an indetermined chord, we treat it as if it were a major chord.
                0b0001100000000000 => 0, // This should not happen, we add it here for completeness.
                _ => unimplemented!("Not implemented: {:b}", c.uninvert() as u16),
            },
        }
    }
}

#[test]
fn uninvert_chord_works() {
    assert_eq!(ChordType::Other.uninvert(), ChordType::Other);
    assert_eq!(ChordType::Silence.uninvert(), ChordType::Silence);
    assert_eq!(ChordType::Major.uninvert(), ChordType::Major);
    assert_eq!(ChordType::Dominant7.uninvert(), ChordType::Dominant7);
    assert_eq!(ChordType::Dominant7Inv1.uninvert(), ChordType::Dominant7);
    assert_eq!(ChordType::Dominant7Inv2.uninvert(), ChordType::Dominant7);
    assert_eq!(ChordType::Dominant7Inv3.uninvert(), ChordType::Dominant7);
    assert_eq!(ChordType::MajorInv1.uninvert(), ChordType::Major);
    assert_eq!(ChordType::MajorInv2.uninvert(), ChordType::Major);
    assert_eq!(ChordType::Minor.uninvert(), ChordType::Minor);
    assert_eq!(ChordType::MinorInv1.uninvert(), ChordType::Minor);
    assert_eq!(ChordType::MinorInv2.uninvert(), ChordType::Minor);
    assert_eq!(ChordType::Major7.uninvert(), ChordType::Major7);
    assert_eq!(ChordType::Major7Inv1.uninvert(), ChordType::Major7);
    assert_eq!(ChordType::Major7Inv2.uninvert(), ChordType::Major7);
    assert_eq!(ChordType::Major7Inv3.uninvert(), ChordType::Major7);
    assert_eq!(ChordType::Minor7.uninvert(), ChordType::Minor7);
    assert_eq!(ChordType::Minor7Inv1.uninvert(), ChordType::Minor7);
    assert_eq!(ChordType::Minor7Inv2.uninvert(), ChordType::Minor7);
    assert_eq!(ChordType::Minor7Inv3.uninvert(), ChordType::Minor7);
    assert_eq!(ChordType::Augmented.uninvert(), ChordType::Augmented);
    assert_eq!(ChordType::Diminished.uninvert(), ChordType::Diminished);
    assert_eq!(ChordType::DiminishedInv1.uninvert(), ChordType::Diminished);
    assert_eq!(ChordType::DiminishedInv2.uninvert(), ChordType::Diminished);
    assert_eq!(ChordType::Power.uninvert(), ChordType::Power);
    assert_eq!(ChordType::PowerInv1.uninvert(), ChordType::Power);
}

fn chord_type(chord: u128) -> ChordType {
    if let Some(l) = lowest(chord) {
        let mut wrapped = (wrap_chord(chord) as u32) << 16;
        wrapped = wrapped | (wrapped >> 12);
        wrapped = wrapped << (l % 12);
        wrapped = wrapped & FIRST_12_BITS_U32;
        ChordType::from_u16((wrapped >> 16) as u16).unwrap_or(ChordType::Other)
    } else {
        ChordType::Silence
    }
}

fn note_name(note: u32) -> &'static str {
    const NAMES: [&'static str; 12] = [
        "C", "C♯", "D", "E♭", "E", "F", "F♯", "G", "A♭", "A", "B♭", "B",
    ];
    NAMES[(note % 12) as usize]
}

fn note_name_sharps(note: u32) -> &'static str {
    const NAMES: [&'static str; 12] = [
        "C", "C♯", "D", "D♯", "E", "F", "F♯", "G", "G♯", "A", "A♯", "B",
    ];
    NAMES[(note % 12) as usize]
}

fn note_name_flats(note: u32) -> &'static str {
    const NAMES: [&'static str; 12] = [
        "C", "D♭", "D", "E♭", "E", "F", "G♭", "G", "A♭", "A", "B♭", "B",
    ];
    NAMES[(note % 12) as usize]
}

#[derive(Copy, Clone)]
enum NoteNamePreference {
    Sharps,
    Flats,
    NoPreference,
}

fn note_name_preference(note: u32) -> NoteNamePreference {
    match number_of_sharps_or_flats(note).signum() {
        0 => NoteNamePreference::NoPreference,
        1 => NoteNamePreference::Sharps,
        -1 => NoteNamePreference::Flats,
        _ => unreachable!(),
    }
}

/// Return the number of sharps or flats
/// > 0: the number of sharps
/// < 0: the number of flats
fn number_of_sharps_or_flats(note: u32) -> i32 {
    const MAX_NUMBER_OF_FLATS: i32 = 6;
    // C (corresponds to 0) has zero flats,
    // G has one flat and if you travel the circle of fifths,
    // you always get one flat more.
    // So the number of flats is the number of "steps" you have to take on the circle
    // of fifths.
    // So number_of_sharps_or_flats(note) = number of steps
    //                                    = note / "step size" (mod 12)
    // Now "step size" corresponds to 7 (travel one fifth is one semitone),
    // so number_of_sharps_or_flats(note) = note / 7 (mod 12).
    // Now 7 * 7 = 1 mod 12, so
    //   number_of_sharps_or_flats(note) = note * 7 (mod 12).
    // Of course, we don't want 10 sharps, for instance, so we use flats also.
    // For this, we shift around a little with MAX_NUMBER_OF_FLATS.
    ((((note % 12) * 7) % 12) as i32 + MAX_NUMBER_OF_FLATS).wrapping_rem_euclid(12)
        - MAX_NUMBER_OF_FLATS
}

#[test]
fn number_of_sharps_or_flats_works() {
    assert_eq!(number_of_sharps_or_flats(0), 0); //   C
    assert_eq!(number_of_sharps_or_flats(7), 1); //   G
    assert_eq!(number_of_sharps_or_flats(2), 2); //   D
    assert_eq!(number_of_sharps_or_flats(9), 3); //   A
    assert_eq!(number_of_sharps_or_flats(4), 4); //   E
    assert_eq!(number_of_sharps_or_flats(11), 5); //  B
    assert_eq!(number_of_sharps_or_flats(6), -6); //  Gb
    assert_eq!(number_of_sharps_or_flats(5), -1); //  F
    assert_eq!(number_of_sharps_or_flats(10), -2); // Bb
    assert_eq!(number_of_sharps_or_flats(3), -3); //  Eb
    assert_eq!(number_of_sharps_or_flats(8), -4); //  Ab
    assert_eq!(number_of_sharps_or_flats(1), -5); //  Db
}

fn note_name_with_preference(note: u32, preference: NoteNamePreference) -> &'static str {
    match preference {
        NoteNamePreference::NoPreference => note_name(note),
        NoteNamePreference::Sharps => note_name_sharps(note),
        NoteNamePreference::Flats => note_name_flats(note),
    }
}

#[cfg(test)]
fn note_from_name(name: &str) -> (Option<u32>, &str) {
    let mut chars = name.chars();
    if let Some(n) = chars.next() {
        let result = match n {
            'C' => 0,
            'D' => 2,
            'E' => 4,
            'F' => 5,
            'G' => 7,
            'A' => 9,
            'B' => 11,
            _ => {
                return (None, name);
            }
        };
        let remainder = chars.as_str();
        match chars.next() {
            None => (Some(result), chars.as_str()),
            Some('♯') => (Some((result + 1) % 12), chars.as_str()),
            Some('♭') => (Some((result + 11) % 12), chars.as_str()),
            Some(_) => (Some(result), remainder),
        }
    } else {
        return (None, name);
    }
}

pub fn string_representation(chord: u128) -> String {
    if let Some(l) = lowest(chord) {
        let chord_type = chord_type(chord);
        let root_difference = chord_type.root_difference();
        let root = (12 + l - root_difference) % 12;
        let preference = note_name_preference(root + (12 + chord_type.relative_chord()));
        if root_difference == 0 {
            format!(
                "{}{}",
                note_name_with_preference(root, preference),
                chord_type.suffix()
            )
        } else {
            format!(
                "{}{}/{}",
                note_name_with_preference(root, preference),
                chord_type.suffix(),
                note_name_with_preference(l, preference)
            )
        }
    } else {
        "".to_string()
    }
}

#[test]
fn correct_number_of_sharps_and_flats_applied() {
    let notes = "G♭4 E♭5 B♭5";
    let chord = parse(notes).unwrap();
    assert_eq!(string_representation(chord), "E♭m/G♭");
}
