//     A stand-alone jack application that listens to a midi channel and prints the chord names.
//     Copyright (C) 2021  Pieter Penninckx
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as
//     published by the Free Software Foundation, either version 3 of the
//     License, or (at your option) any later version that is approved by Pieter Penninckx.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
//! # Print-chords
//! A stand-alone jack application that listens to a midi channel and prints the chord names.
//!
//! # Usage
//! See the [command line options](`Options`).
#![deny(non_snake_case)]
#[macro_use]
extern crate enum_primitive;

mod chord_listener;
mod chord_printer;

use crate::chord_listener::MultiChannelChordListener;
use anyhow::{anyhow, Context, Result};
use chord_printer::string_representation;
use jack::{
    Client, ClientOptions, Control, MidiIn, Port, ProcessHandler, RingBufferReader,
    RingBufferWriter,
};
use midi_consts::channel_event::control_change::{ALL_NOTES_OFF, ALL_SOUND_OFF};
use midi_consts::channel_event::{CONTROL_CHANGE, EVENT_TYPE_MASK, NOTE_OFF, NOTE_ON};
use std::env;
use std::io::{self, Write};
use std::thread::spawn;

const USAGE: &'static str = include_str!("../USAGE.txt");
const FIRST_BIT: u128 = (0b1000000000000000 as u128) << (128 - 16);

const PORT_NAME: &'static str = "midi in";

struct JackChordListener {
    inner: MultiChannelChordListener,
    writer: RingBufferWriter,
    previous_chord: u128,
    midi_in_port: Port<MidiIn>,
}

impl JackChordListener {
    pub fn new(writer: RingBufferWriter, midi_in_port: Port<MidiIn>) -> Self {
        Self {
            inner: MultiChannelChordListener::new_with_all_channels(),
            writer,
            previous_chord: 0,
            midi_in_port,
        }
    }
}

impl ProcessHandler for JackChordListener {
    fn process(&mut self, _client: &Client, process_scope: &jack::ProcessScope) -> jack::Control {
        let mut must_print_chord = false;
        for raw_midi in self.midi_in_port.iter(process_scope) {
            let data = raw_midi.bytes;
            if data.len() >= 3 {
                match (data[0] & EVENT_TYPE_MASK, data[1]) {
                    (NOTE_OFF, _)
                    | (NOTE_ON, _)
                    | (CONTROL_CHANGE, ALL_NOTES_OFF)
                    | (CONTROL_CHANGE, ALL_SOUND_OFF) => {
                        self.inner.handle_event(data);
                    }
                    _ => {}
                }
                let chord = self.inner.get_chord();
                if chord.count_ones() > self.previous_chord.count_ones() {
                    must_print_chord = true;
                }
                self.previous_chord = chord;
            }
        }
        if must_print_chord {
            self.writer
                .write_buffer(&self.previous_chord.to_ne_bytes()[..]);
        }
        Control::Continue
    }
}

struct JackChordPrinter {
    reader: RingBufferReader,
}

impl JackChordPrinter {
    fn new(reader: RingBufferReader) -> Self {
        Self { reader }
    }

    fn run(&mut self) {
        let mut buffer = [0_u8; 16];
        loop {
            let mut chord = 0;
            let mut found = false;
            while self.reader.space() >= 16 {
                self.reader.read_buffer(&mut buffer[..]);
                chord = u128::from_ne_bytes(buffer);
                found = true;
            }
            if found {
                print!("\r{:10}", string_representation(chord));
                let _ = std::io::stdout().flush();
            }
            std::thread::sleep(std::time::Duration::from_millis(10));
        }
    }
}

pub struct Options {
    print_help: bool,
}

impl Default for Options {
    fn default() -> Self {
        Self { print_help: false }
    }
}

impl Options {
    fn from_command_line_arguments() -> Result<Self> {
        let mut args: Vec<String> = env::args().rev().collect();
        let _ = args.pop();
        let mut result = Self::default();
        while let Some(arg) = args.pop() {
            match arg.as_str() {
                "--help" | "-h" => {
                    result.print_help = true;
                }
                a => return Err(anyhow!("Unknown command line argument: {}", a)),
            }
        }
        Ok(result)
    }
}

fn main() -> Result<()> {
    let options = Options::from_command_line_arguments()?;
    if options.print_help {
        println!("{}", USAGE);
        return Ok(());
    }
    let ringbuf = jack::RingBuffer::new(1024).context("Error while creating jack ringbuffer.")?;
    let (reader, writer) = ringbuf.into_reader_writer();
    let (client, _client_status) = Client::new("print_chords", ClientOptions::empty())
        .context("Error while registering Jack client.")?;
    let port = client
        .register_port(PORT_NAME, MidiIn)
        .context("Error while registering midi in port.")?;
    let listener = JackChordListener::new(writer, port);
    let mut printer = JackChordPrinter::new(reader);
    let async_client = client
        .activate_async((), listener)
        .context("Error while running or starting jack client.")?;
    spawn(move || printer.run());
    println!("Press enter to quit");
    let mut user_input = String::new();

    io::stdin().read_line(&mut user_input).ok();

    async_client
        .deactivate()
        .context("Error while deactivating client.")?;
    Ok(())
}
